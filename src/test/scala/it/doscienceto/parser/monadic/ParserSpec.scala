package it.doscienceto.parser.monadic

import cats.syntax.all._
import it.doscienceto.parser.ParseFailure
import org.scalatest.freespec.AnyFreeSpec
import org.scalatest.matchers.should.Matchers

class ParserSpec extends AnyFreeSpec with Matchers {

  case class Person(isTall: Boolean, firstname: String, surname: String)

  val personParser: Parser[Person] = for {
    tall <- Flag("tall")
    firstname <- StringArg("firstname")
    surname <- StringArg("surname")
  } yield Person(tall, firstname, surname)

  "A Monadic Parser" - {
    "Should be able to parse argument lists" - {
      "basic clean parse" in {
        val args = "--tall --firstname John --surname Doe".split(' ').toList
        personParser.parse(args) should be(Right(Person(true, "John", "Doe")))
      }
      "flags are optional" in {
        val args = "--firstname John --surname Doe".split(' ').toList
        personParser.parse(args) should be(Right(Person(false, "John", "Doe")))
      }
      "arguments are not optional" in {
        val args = "--tall --surname Doe".split(' ').toList
        personParser.parse(args) should be(Left(ParseFailure("Missing argument --firstname")))
      }
    }
    "can construct parsers that are more complex than applicative parsers" - {
      sealed trait Person {
        def firstname: String
      }
      case class PersonWithoutSurname(firstname: String) extends Person
      case class PersonWithSurname(firstname: String, surname: String) extends Person

      val personParser = for {
        hasSurname <- Flag("has-surname")
        firstname <- StringArg("firstname")
        person: Person <- if (hasSurname) {
          StringArg("surname").map( surname =>
            PersonWithSurname(firstname, surname)
          )
        } else {
          PersonWithoutSurname(firstname).pure[Parser]
        }
      } yield person

      "can parse argument argument lists" in {
        val argsWithSurname= "--has-surname --firstname John --surname Doe".split(' ').toList
        personParser.parse(argsWithSurname) should be(Right(PersonWithSurname("John", "Doe")))

        val argsWithoutSurname= "--firstname John".split(' ').toList
        personParser.parse(argsWithoutSurname) should be(Right(PersonWithoutSurname("John")))
      }
      "can vary the parser used based on the previous parser" in {
        val invalidArgs= "--has-surname --firstname John".split(' ').toList
        personParser.parse(invalidArgs) should be(Left(ParseFailure("Missing argument --surname")))
      }
    }
  }
}

