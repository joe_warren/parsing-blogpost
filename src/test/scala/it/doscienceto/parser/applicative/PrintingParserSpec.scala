package it.doscienceto.parser.applicative

import org.scalatest.freespec.AnyFreeSpec
import cats.syntax.all._
import org.scalatest.matchers.should.Matchers

class PrintingParserSpec extends AnyFreeSpec with Matchers {
  case class Person(tall: Boolean, firstname: String, surname: String)

  val personParser = (
      Flag("tall"),
      StringOption("firstname"),
      StringOption("surname")
    ).mapN(Person)

  "A printing Parser" - {
    "should be able to parse arguments like the previous parser" in {
      val args = "--tall --firstname John --surname Doe".split(' ').toList
      personParser.parse(args) should be(Right(Person(true, "John", "Doe")))
    }
    "should also be able to print a usage line" in {
      personParser.usage should be("[--tall] --firstname <value> --surname <value>")

    }
  }
}
