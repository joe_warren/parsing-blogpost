package it.doscienceto.parser.applicative

import cats.Applicative
import it.doscienceto.parser.ParseFailure

sealed abstract class PrintingParser[A] {
    type PartialParseResult = Either[ParseFailure, (List[String], A)]

    protected def partialParse(args: List[String]): PartialParseResult

    final def parse(args: List[String]): Either[ParseFailure, A] = partialParse(args) flatMap {
      case (List(), result) => Right(result)
      case (remainingArgs, _) => Left(ParseFailure(s"Unrecognised Arguments:${remainingArgs.mkString("\t\n", "\t\n", "")}"))
    }

    def usage: String
}

object Flag {
  def apply(name: String): PrintingParser[Boolean] = new PrintingParser[Boolean] {
    private val flag = s"--$name"

    override protected def partialParse(args: List[String]): PartialParseResult = {
      Right(args.filter(_ != flag), args.contains(flag))
    }

    override def usage: String = s"[$flag]"
  }
}

object StringOption {
  def apply(name: String): PrintingParser[String] = new PrintingParser[String] {
    private val flag = s"--$name"

    override protected def partialParse(args: List[String]): PartialParseResult = {
      println(args.span(_ != flag))
      args.span(_ != flag) match {
        case (_, List()) => Left(ParseFailure(s"Missing argument $flag"))
        case (_, List(_)) => Left(ParseFailure(s"Expected a value after $flag"))
        case (prefix, _ :: value :: suffix) => Right((prefix ++ suffix, value))
      }
    }

    override def usage: String = s"--$name <value>"
  }
}



object PrintingParser {
  implicit def applicativeForPrintingParser: Applicative[PrintingParser] = new Applicative[PrintingParser] {
    override def pure[A](x: A): PrintingParser[A] = new PrintingParser[A] {
      override protected def partialParse(args: List[String]): PartialParseResult =  Right((args, x))
      override def usage: String = ""
    }

    override def ap[A, B](ff: PrintingParser[A => B])(fa: PrintingParser[A]): PrintingParser[B] = new PrintingParser[B] {
      override protected def partialParse(args: List[String]): PartialParseResult = for {
        (nextArgs, a) <- fa.partialParse(args)
        (nextNextArgs, function) <- ff.partialParse(nextArgs)
      } yield (nextNextArgs, function(a))

      // If we were to join both usage strings together with a space, this would break the Monad identity laws
      // Adding no space if either string is empty makes the Applicative lawful (to the best of my understanding)
      override def usage: String = (ff.usage, fa.usage) match {
        case ("", "") => ""
        case ("", snd) => snd
        case (fst, "") => fst
        case (fst, snd) => s"$fst $snd"
      }
    }
  }
}
