package it.doscienceto.parser.monadic

import cats.Monad
import it.doscienceto.parser.ParseFailure

/* Making the class sealed prevents other users from creating their `Parser` subclasses
 * Our aim is to provide simple buildng blocks that can be combined into a more complex parser
 *  not to provide an interface that they have to implement
 *
 * The class is parameterized with a type `A`, this is the type of object produced by a successful parse.
 */
sealed abstract class Parser[A] {
  // defining a type alias saves us some typing
  type PartialParseResult = Either[ParseFailure, (List[String], A)]

  // Each parser can attempt to partially parse a block of command line arguments
  // This will either return a `Left[ParseFailure]` if the arguments are invalid
  // or it will "consume" some of the arguments, returning a `Right[(List[String], A)]`
  // containing both a reduced list of arguments, and a value of type `A`
  //
  // We can mark this protected, because it isn't part of the external dsl
  protected def partialParse(args: List[String]): PartialParseResult

  // we parse a list of arguments by calling `partialParse`,
  // and then validating that there are no arguments left unconsumed
  // every Parser should implement `parse` in the same way, so we can mark this `final`
  final def parse(args: List[String]): Either[ParseFailure, A] = partialParse(args) flatMap {
    case (List(), result) => Right(result)
    case (remainingArgs, _) => Left(ParseFailure(s"Unrecognised Arguments:${remainingArgs.mkString("\t\n", "\t\n", "")}"))
  }
}


object Flag {
  def apply(name: String): Parser[Boolean] = new Parser[Boolean] {
    private val flag = s"--$name"

    // a flag is either present, or it is absent, both valid states, so `partialParse can't fail
    // and always returns a `Right`
    // The first value in the tuple is the arguments list with the flag (if present) removed
    // The seconf value is a boolean; whether the flag was passed
    override protected def partialParse(args: List[String]): PartialParseResult =
      Right(args.filter(_ != flag), args.contains(flag))
  }
}

object StringArg {
  def apply(name: String): Parser[String] = new Parser[String] {
    private val flag = s"--$name"

    override protected def partialParse(args: List[String]): PartialParseResult = {
      println(args.span(_ != flag))
      args.span(_ != flag) match {
        case (_, List()) => Left(ParseFailure(s"Missing argument $flag"))
        case (_, List(_)) => Left(ParseFailure(s"Expected a value after $flag"))
        case (prefix, _ :: value :: suffix) => Right((prefix ++ suffix, value))
      }
    }
  }
}

object Parser {
  implicit def monadForParser: Monad[Parser] = new Monad[Parser] {
    // pure creates a parser that always produces the same value, no matter what the input is
    override def pure[A](x: A): Parser[A] = new Parser[A] {
      override protected def partialParse(args: List[String]): Either[ParseFailure, (List[String], A)] = Right((args, x))
    }

    // flatMap runs the first parser
    // then calls the provided function on the output to create a new parser
    // and runs that on the arguments that weren't consumed by the first parser
    // this implementation uses the `Either` Monad to compose the potential errors from the two parsers
    override def flatMap[A, B](fa: Parser[A])(f: A => Parser[B]): Parser[B] = new Parser[B] {
      override protected def partialParse(args: List[String]): PartialParseResult = for {
        (nextArgs, a) <- fa.partialParse(args)
        result <- f(a).partialParse(nextArgs)
      } yield result
    }

    // cats Monad requires we write tailRecM for reasons of stack safety
    // Because this is example code, lets more or less ignore it, and go with a stock implementation
    override def tailRecM[A, B](a: A)(f: A => Parser[Either[A, B]]): Parser[B] =
      flatMap(f(a)) {
        case Right(b) => pure(b)
        case Left(nextA) => tailRecM(nextA)(f)
      }
  }
}

