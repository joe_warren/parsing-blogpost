package it.doscienceto.parser

case class ParseFailure(error: String)
